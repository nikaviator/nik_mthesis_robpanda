# A Complete Panda Gazebo Workspace

## Requirements

- Ubuntu 18.4
- ROS Melodic

## Installation

- install ROS package dependencies `rosdep install --from-paths .`
- build the workspace `catkin build`
- source the config: depending on your shell
    - `source devel/setup.bash` (default)
- run the simulation `roslaunch panda_simulation simulation.launch`



